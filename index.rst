.. SatNOGS kit documentation master file, created by
   sphinx-quickstart on Wed May  8 17:46:14 2019.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

Welcome to SatNOGS kit's documentation!
=======================================

SatNOGS kit is a collection of components for building a reference SatNOGS ground station.
This documentation is a hardware components and materials specification.

.. toctree::
   :maxdepth: 2


Indices and tables
==================

* :ref:`genindex`
* :ref:`search`
